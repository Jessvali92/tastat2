package tastat;

import java.util.Date;

public class DaemonReposador extends Thread {

	public Magatzem magatzem = new Magatzem();

	public DaemonReposador() {
		this.magatzem = Programa.getMagatzem();
	}

	@Override
	public void run() {
		while (true) {
			try {
				sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			revisarOrdres(magatzem);
		}
	}

	/**
	 * Revisa la llista de ordres de compra y si no esta buida executa la primera.
	 * 
	 * @param magatzeml
	 */
	private void revisarOrdres(Magatzem magatzeml) {
		if (!magatzeml.getCompres().isEmpty()) {
			OrdreCompra oc = magatzeml.getCompres().get(0);
			if (oc.getEstat().equals(OrdreEstat.PENDENT)) {
				for (Producte p : magatzeml.getProductes()) {
					if (oc.getProducte().equals(p)) {
						p.afegirLot(oc.getQuantitat(), new Date());
						p.setStock(oc.getQuantitat() + p.getStock());
						magatzeml.getMoviments()
								.add(new DiariMoviments("Ordre de compra " + oc.getIdOrdre() + " executada -> "
										+ oc.getQuantitat() + " " + oc.getProducte().getUnitatMesura().toString()
										+ " de " + oc.getProducte().getNomProducte() + " afegits al magatzem"));
						System.out.println("Ordre de compra " + oc.getIdOrdre() + " executada -> " + oc.getQuantitat()
								+ " " + oc.getProducte().getUnitatMesura().toString() + " afegits al magatzem");
						// magatzeml.getMoviments().add(new DiariMoviments(p, lot, 'C',
						// oc.getQuantitat(),
						// "ENTRAN " + oc.getQuantitat() + " " + p.getNomProducte()));
						oc.setEstat(OrdreEstat.LLIURADA);
					}
				}
			}
		}
	}
}
