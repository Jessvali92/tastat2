package tastat;

public class DaemonTraumat extends Thread {

	Magatzem mgz = new Magatzem();
	int nRandom = 0;
	int contadorVeces = 0;
	private boolean fi = false;

	public DaemonTraumat(Magatzem mgz) {
		this.mgz = mgz;
	}

	@Override
	public void run() {
		while (!fi) {
			try {
				sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			banearOperariosPorqueRobanDroja();
		}
	}

	private void banearOperariosPorqueRobanDroja() {
		if (getMagatzem().getOperaris().size() > 0) {
			nRandom = (int) (Math.random() * mgz.getOperaris().size());
			Operari o = mgz.getOperaris().get(nRandom);
			if (contadorVeces == 5) {
				o.vol = true;
				System.out.println("Traumat: " + o.getNomOperari() + " Despedido !");
				getMagatzem().getMoviments()
						.add(new DiariMoviments("Traumat acomiada a l'operari " + o.getNomOperari()));
				getMagatzem().getOperaris().remove(o);
				contadorVeces = 0;
			} else {
				getMagatzem().getMoviments().add(
						new DiariMoviments("Traumat revisa a l'operari " + o.getNomOperari() + " pero el deixa anar"));
				System.out.println("Traumat: " + o.getNomOperari() + " te puedes ir por ahora...");
				contadorVeces++;
			}
		} else {
			this.fi = true;
		}
	}

	public Magatzem getMagatzem() {
		return Programa.getMagatzem();
	}
}
