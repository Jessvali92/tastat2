package tastat;

import java.util.Date;

public class DiariMoviments {
	protected int numMoviment;
	protected Date moment;
	protected Producte producte;
	protected int lot;
	protected int quantitat;
	protected String observacions;
	protected Comanda comanda;
	protected OrdreCompra ordreCompra;

	public String toString() {
		return observacions + "\n";

	}

	DiariMoviments( String observacions) {
		this.observacions = observacions;
	}

	DiariMoviments() {
		numMoviment = GeneradorIDs.getNextDiariMoviment();
		moment = new Date();
	}

	DiariMoviments(Producte p, int l, int q, String obs) {
		this();
		producte = p;
		lot = l;
		observacions = obs;
	}

	public int getNumMoviment() {
		return numMoviment;
	}

	public void setNumMoviment(int numMoviment) {
		this.numMoviment = numMoviment;
	}

	public Date getMoment() {
		return moment;
	}

	public void setMoment(Date moment) {
		this.moment = moment;
	}

	public Producte getProducte() {
		return producte;
	}

	public void setProducte(Producte producte) {
		this.producte = producte;
	}

	public int getLot() {
		return lot;
	}

	public void setLot(int lot) {
		this.lot = lot;
	}

	public int getQuantitat() {
		return quantitat;
	}

	public void setQuantitat(int quantitat) {
		this.quantitat = quantitat;
	}

	public String getObservacions() {
		return observacions;
	}

	public void setObservacions(String observacions) {
		this.observacions = observacions;
	}

	public Comanda getComanda() {
		return comanda;
	}

	public void setComanda(Comanda comanda) {
		this.comanda = comanda;
	}

	public OrdreCompra getOrdreCompra() {
		return ordreCompra;
	}

	public void setOrdreCompra(OrdreCompra ordreCompra) {
		this.ordreCompra = ordreCompra;
	}

}