package tastat;

public class DaemonMonitor extends Thread {

	public Magatzem magatzem = new Magatzem();

	public DaemonMonitor() {
		this.magatzem = Programa.getMagatzem();
	}

	@Override
	public void run() {
		while (true) {
			try {
				sleep(30000);
				imprimirEstados(magatzem);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void imprimirEstados(Magatzem magatzeml) {

		System.out.println("\n---------------------ESTAT DE COMANDES---------------------------------\n");
		for (int i = 0; i < magatzeml.getComandes().size(); i++) {
			if (magatzeml.getComandes().get(i).getEstat().equals(ComandaEstat.LLIURADA)) {
				System.out.println("\nComanda " + magatzeml.getComandes().get(i).getIdComanda() + " -> en TRANSPORT");
			} else if (magatzeml.getComandes().get(i).getEstat().equals(ComandaEstat.TRANSPORT)) {
				System.out.println("\nComanda " + magatzeml.getComandes().get(i).getIdComanda() + " -> LLIURADA");
			} else if (magatzeml.getComandes().get(i).getEstat().equals(ComandaEstat.PREPARADA)) {
				System.out.println("\nComanda " + magatzeml.getComandes().get(i).getIdComanda() + " -> PREPARADA");
			} else {
				System.out.println("\nComanda " + magatzeml.getComandes().get(i).getIdComanda() + " -> PENDENT");
				System.out.println(magatzeml.getComandes().get(i).toString());
			}
		}

		System.out.println("\nOperaris:");
		for (Operari o : magatzeml.getOperaris()) {
			if (o.isDescanso()) {
				System.out.println("\t" + o.getNomOperari() + "-> Descansant");
			} else {
				if (o.isFabricant()) {
					System.out.println("\t" + o.getNomOperari() + "-> Treballant");
				} else {
					System.out.println("\t" + o.getNomOperari() + "-> En espera");
				}
			}
		}

		System.out.println("\nOrdres de compra pendents:");
		for (OrdreCompra oc : magatzeml.getCompres()) {
			System.out.print("\n" + oc.getProducte().getNomProducte());
			System.out.print("\t" + oc.getQuantitat() + " " + oc.getProducte().getUnitatMesura().toString());
		}
		System.out.println("\n");
	}
}
