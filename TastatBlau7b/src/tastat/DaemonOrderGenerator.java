package tastat;

public class DaemonOrderGenerator extends Thread {

	Comanda comandaCreada;
	Magatzem mgz = new Magatzem();
	int nRandom = 0;
	boolean noA�adir = false;

	public DaemonOrderGenerator() {
		this.mgz = Programa.getMagatzem();
	}

	@Override
	public void run() {
		Producte pliv = null, pllim = null, peri = null, pvel = null;
		for (Producte p : mgz.getProductes()) {
			if (p.getNomProducte().equals("pLiviano"))
				pliv = p;
			if (p.getNomProducte().equals("pLlimona"))
				pllim = p;
			if (p.getNomProducte().equals("pErizo"))
				peri = p;
			if (p.getNomProducte().equals("pVelvet"))
				pvel = p;
		}
		while (true) {
			try {
				sleep(20000);
				System.out.println("-----------------Generant noves comandes-----------------");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			int nC = (int) (Math.random() * mgz.getClients().size());
			Client c = mgz.getClients().get(nC);
			Comanda m1 = new Comanda(c);
			while (m1.getLinies().isEmpty()) {
				nRandom = (int) (Math.random() * 10);
				if (nRandom != 0) {
					m1.getLinies().add(new ComandaLinia(pliv, nRandom));
				}
				nRandom = (int) (Math.random() * 10);
				if (nRandom != 0) {
					m1.getLinies().add(new ComandaLinia(pllim, nRandom));
				}
				nRandom = (int) (Math.random() * 10);
				if (nRandom != 0) {
					m1.getLinies().add(new ComandaLinia(peri, nRandom));
				}
				nRandom = (int) (Math.random() * 10);
				if (nRandom != 0) {
					m1.getLinies().add(new ComandaLinia(pvel, nRandom));
				}
			}
			mgz.getComandes().add(m1);
		}
	}
}
