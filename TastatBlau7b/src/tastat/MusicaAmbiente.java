package tastat;

import javax.swing.*;
import sun.audio.*;
import java.io.*;

public class MusicaAmbiente extends Thread {

	private String music;

	public MusicaAmbiente(String filepath) {
		this.music = filepath;
	}

	@Override
	public void run() {

		while (true) {
			try {
				playMusic(music);
				sleep(90000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void playMusic(String filepath) {

		InputStream music;
		try {
			music = new FileInputStream(new File(filepath));
			AudioStream audios = new AudioStream(music);
			AudioPlayer.player.start(audios);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Para reproducir la musica cambia la ruta del archivo narcos.wav");
		}
	}
}
