package tastat;

import java.util.concurrent.Semaphore;

public class ComandaLinia {
	private Producte producte;
	private double preuVenda;
	private int quantitatPreparada;
	private int quantitatPreparacio;
	private int quantitatDemanada;
	private Semaphore semaforo = new Semaphore(1);

	ComandaLinia(Producte p, int q, double preu) {
		producte = p;
		quantitatDemanada = q;
		preuVenda = preu;
		quantitatPreparada = 0;
		setQuantitatPreparacio(0);
	}

	ComandaLinia(Producte p, int q) {
		producte = p;
		quantitatDemanada = q;
		preuVenda = p.getPreuVenda();
		quantitatPreparada = 0;
		setQuantitatPreparacio(0);
	}

	public Producte getProducte() {
		return producte;
	}

	public void setProducte(Producte producte) {
		this.producte = producte;
	}

	public double getPreuVenda() {
		return preuVenda;
	}

	public void setPreuVenda(double preuVenda) {
		this.preuVenda = preuVenda;
	}

	public int getQuantitatPreparada() {
		return quantitatPreparada;
	}

	public void setQuantitatPreparada(int quantitatPreparada) {
		this.quantitatPreparada = quantitatPreparada;
	}

	public int getQuantitatPreparacio() {
		return quantitatPreparacio;
	}

	public void setQuantitatPreparacio(int quantitatPreparacio) {
		this.quantitatPreparacio = quantitatPreparacio;
	}

	public int getQuantitatDemanada() {
		return quantitatDemanada;
	}

	public void setQuantitatDemanada(int quantitatDemanada) {
		this.quantitatDemanada = quantitatDemanada;
	}

	@Override
	public String toString() {
		String s = "\nProducte: " + this.getProducte().getNomProducte() + " -> QuantitatPreparada  "
				+ this.getQuantitatPreparada() + "/" + this.getQuantitatDemanada();
		return s;
	}

	public Semaphore getSemaforo() {
		return semaforo;
	}

	public void setSemaforo(Semaphore semaforo) {
		this.semaforo = semaforo;
	}
}