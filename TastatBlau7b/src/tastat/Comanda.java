package tastat;

import java.util.ArrayDeque;
import java.util.Date;

public class Comanda {

	protected int idComanda;
	protected Client client;
	protected Date dataComanda;
	protected Date dataLliurament;
	protected ComandaEstat estat; // PENDENT - PREPARAT - TRANSPORT - LLIURAT
	protected Double portes; // preu de transport
	protected ArrayDeque<ComandaLinia> linies;

	Comanda() {
		idComanda = GeneradorIDs.getNextComanda();
		dataComanda = new Date();
		dataLliurament = Tools.sumarDies(new Date(), 1);
		estat = ComandaEstat.PENDENT;
		portes = 0.0;
		linies = new ArrayDeque<ComandaLinia>();
	}

	Comanda(Client client) {
		this();
		this.client = client;
	};

	/**
	 * 
	 * @return
	 */
	public Producte ObtenirSeguentProducte() {
		for (ComandaLinia cl : this.getLinies()) {
			try {
				cl.getSemaforo().acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if ((cl.getQuantitatPreparada() + cl.getQuantitatPreparacio()) < cl.getQuantitatDemanada()) {
				cl.setQuantitatPreparacio(cl.getQuantitatPreparacio() + 1);
				cl.getSemaforo().release();
				return cl.getProducte();
			}
			cl.getSemaforo().release();
		}
		System.out.println("¡ObtenirSeguentProducte ha retornado null!");
		return null;

	}

	public int getIdComanda() {
		return idComanda;
	}

	public void setIdComanda(int idComanda) {
		this.idComanda = idComanda;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Date getDataComanda() {
		return dataComanda;
	}

	public void setDataComanda(Date dataComanda) {
		this.dataComanda = dataComanda;
	}

	public Date getDataLliurament() {
		return dataLliurament;
	}

	public void setDataLliurament(Date dataLliurament) {
		this.dataLliurament = dataLliurament;
	}

	public ComandaEstat getEstat() {
		return estat;
	}

	public void setEstat(ComandaEstat estat) {
		this.estat = estat;
	}

	public Double getPortes() {
		return portes;
	}

	public void setPortes(Double portes) {
		this.portes = portes;
	}

	public void setLinies(ArrayDeque<ComandaLinia> linies) {
		this.linies = linies;
	}

	public ArrayDeque<ComandaLinia> getLinies() {
		return linies;
	}

	@Override
	public String toString() {
		String cadena = "\nComanda: " + getIdComanda() + " Client: " + getClient().getNomClient() + " " + getEstat()
				+ " linies:";
		for (ComandaLinia cl : getLinies()) {
			cadena += cl.toString();
		}
		cadena += "\n\n------------------------------------------------------------";
		return cadena;
	}
}