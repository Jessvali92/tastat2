package tastat;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Magatzem {

	private List<Producte> magatzem = new ArrayList<Producte>();
	private List<Client> clients = new ArrayList<Client>();
	private List<Comanda> comandes = new ArrayList<Comanda>();
	private List<Comanda> comandesFinalitzades = new ArrayList<Comanda>();
	private List<Proveidor> proveidors = new ArrayList<Proveidor>();
	private List<Operari> operaris = new ArrayList<Operari>();
	private List<DiariMoviments> moviments = Collections.synchronizedList(new ArrayList<DiariMoviments>());
	private List<OrdreCompra> compres = new ArrayList<OrdreCompra>();
	private List<Comanda> comandesEnPreparacio = new ArrayList<Comanda>();
	private List<Operari> operarisEsperenComandes = new ArrayList<Operari>();
	private Semaphore semaforoDiariMoviments = new Semaphore(1);
	private Semaphore semaforoOrdresCompra = new Semaphore(1);
	private Semaphore sfReguladorOperarios = new Semaphore(12);
	public final int MAXIM_COMANDES_EN_PREPARACIO = 3;

	Magatzem() {
	}

	Magatzem(List<Producte> lp, List<Client> lc, List<Comanda> lm, List<Proveidor> lpv, List<Operari> lop,
			List<DiariMoviments> ldm, List<OrdreCompra> loc) {
		magatzem = lp;
		clients = lc;
		comandes = lm;
		proveidors = lpv;
		operaris = lop;
		moviments = ldm;
		compres = loc;
	}

	boolean afegirQuantitatProducte(int codiProducte, int quantitat) {
		boolean trobat = false;
		for (Producte p : magatzem) {
			if (p.getCodiProducte() == codiProducte) {
				p.setStock(p.getStock() + quantitat);
				trobat = true;
				break;
			}
		}
		return trobat;
	}

	boolean afegirProducte(Producte prod) {
		boolean trobat = false;
		for (Producte p : magatzem) {
			if (p.getCodiProducte() == prod.getCodiProducte()) {
				trobat = true;
				break;
			}
		}
		if (trobat)
			return false;
		else {
			magatzem.add(prod);
			return true;
		}
	}

	public List<Producte> getMagatzem() {
		return magatzem;
	}

	public void setMagatzem(List<Producte> magatzem) {
		this.magatzem = magatzem;
	}

	public List<DiariMoviments> getMoviments() {
		return moviments;
	}

	public void setMoviments(List<DiariMoviments> moviments) {
		this.moviments = moviments;
	}

	public List<OrdreCompra> getCompres() {
		return compres;
	}

	public void setCompres(List<OrdreCompra> compres) {
		this.compres = compres;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public void setComandes(List<Comanda> comandes) {
		this.comandes = comandes;
	}

	public void setProveidors(List<Proveidor> proveidors) {
		this.proveidors = proveidors;
	}

	public void setOperaris(List<Operari> operaris) {
		this.operaris = operaris;
	}

	public List<Producte> getProductes() {
		return magatzem;
	}

	public List<Client> getClients() {
		return clients;
	}

	public List<Comanda> getComandes() {
		return comandes;
	}

	public List<Proveidor> getProveidors() {
		return proveidors;
	}

	public List<Operari> getOperaris() {
		return operaris;
	}

	public Deque<LotDesglossat> apilarCaducats() {
		Iterator<LotDesglossat> it = null;
		Deque<LotDesglossat> pila = new ArrayDeque<LotDesglossat>();
		LotDesglossat ldg;
		Date avui = new Date();
		for (Producte p : magatzem) {
			it = p.lots.iterator();
			while (it.hasNext()) {
				ldg = it.next();
				if (ldg.getDataCaducitat().compareTo(avui) < 0) {
					pila.push((ldg));
					it.remove();
				}
			}
		}
		return pila;
	}

	public boolean add(Producte p) {
		magatzem.add(p);
		return true;
	}

	@Override
	public String toString() {
		String s = "";

		for (Producte p : magatzem)
			s += "\n" + p;
		s += "\nTotal " + magatzem.size() + " Referčncies";
		return s;
	}

	public void veureComandes() {
		for (Comanda c : this.getComandes()) {
			System.out.println(c);
		}
	}

	/**
	 * Permite obtener una comanda pendiente de la lista de comandas pendientes en
	 * el almacen que el operario tenga asignado
	 * 
	 * @return comanda o null si no quedan comandas pendientes en la lista
	 */
	public Comanda ObtenirComandaPendent() {
		for (Comanda c : this.getComandesEnPreparacio()) {
			for (ComandaLinia cl : c.getLinies()) {
				if ((cl.getQuantitatPreparada() + cl.getQuantitatPreparacio()) < cl.getQuantitatDemanada()) {
					return c;
				}
			}
		}
		return null;
	}

	public List<Comanda> getComandesEnPreparacio() {
		return comandesEnPreparacio;
	}

	public void setComandesEnPreparacio(List<Comanda> comandesEnPreparacio) {
		this.comandesEnPreparacio = comandesEnPreparacio;
	}

	public List<Comanda> getComandesFinalitzades() {
		return comandesFinalitzades;
	}

	public void setComandesFinalitzades(List<Comanda> comandesFinalitzades) {
		this.comandesFinalitzades = comandesFinalitzades;
	}

	public List<Operari> getOperarisEsperenNotificacio() {
		return operarisEsperenComandes;
	}

	public void setOperarisEsperenNotificacio(List<Operari> operarisEsperenNotificacio) {
		this.operarisEsperenComandes = operarisEsperenNotificacio;
	}

	public List<Operari> getOperarisEsperenComandes() {
		return operarisEsperenComandes;
	}

	public void setOperarisEsperenComandes(List<Operari> operarisEsperenComandes) {
		this.operarisEsperenComandes = operarisEsperenComandes;
	}

	public Semaphore getSemaforoDiariMoviments() {
		return semaforoDiariMoviments;
	}

	public void setSemaforoDiariMoviments(Semaphore semaforoDiariMoviments) {
		this.semaforoDiariMoviments = semaforoDiariMoviments;
	}

	public int getMAXIM_COMANDES_EN_PREPARACIO() {
		return MAXIM_COMANDES_EN_PREPARACIO;
	}

	public Semaphore getSemaforoOrdresCompra() {
		return semaforoOrdresCompra;
	}

	public void setSemaforoOrdresCompra(Semaphore semaforoOrdresCompra) {
		this.semaforoOrdresCompra = semaforoOrdresCompra;
	}

	public Semaphore getSemaforoFabricantSimultaneament() {
		return sfReguladorOperarios;
	}

	public void setSemaforoFabricantSimultaneament(Semaphore semaforoFabricantSimultaneament) {
		this.sfReguladorOperarios = semaforoFabricantSimultaneament;
	}
}