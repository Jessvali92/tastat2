package tastat;

public class Operari extends Thread {

	protected int idOperari;
	protected String nomOperari;
	protected int numPastissos;
	protected volatile boolean vol = false;
	private volatile boolean descanso = false;
	private boolean fabricant = false;

	Operari(String nom) {
		idOperari = GeneradorIDs.getNextOperari();
		this.nomOperari = nom;
		numPastissos = 0;
	}

	@SuppressWarnings("static-access")
	@Override
	public void run() {

		while (!vol) {
			if (!descanso) {
				Comanda comanda = getMagatzem().ObtenirComandaPendent();
				if (comanda != null) {
					Producte p = comanda.ObtenirSeguentProducte();

					if (p != null) {
						for (ProducteFabricacio fase : p.getFabricacio()) {

							while (!agafarIngredients(fase)) {
								// Si no hay unidades suficientes del ingrediente crea una orden de compra y
								// espera a que llegue
								System.out.println(this.nomOperari + ": Falta ingredient per fase: " + fase.toString());
								boolean yaDemanat = false;
								for (OrdreCompra compra : getMagatzem().getCompres()) {
									if (compra.getProducte().getCodiProducte() == fase.getProducte()
											.getCodiProducte()) {
										yaDemanat = true;
										compra.getOperarisEnEspera().add(this);
									}
								}

								if (!yaDemanat) {
									getMagatzem().getCompres()
											.add(new OrdreCompra(p.getProveidor(), p, p.getQuantitatCompra()));
									for (OrdreCompra compra : getMagatzem().getCompres()) {
										if (compra.getProducte().getCodiProducte() == fase.getProducte()
												.getCodiProducte()) {
											compra.getOperarisEnEspera().add(this);
										}
									}
								}

								try {
									this.sleep(2000);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}

							// Cuando tiene el ingrediente espera por el tiempo que tarde la fase
							long tempsFase = (long) (Math.random() * (fase.getTempsMax() - fase.getTempsMin())
									+ fase.getTempsMin());
							// Multipliquem per 100 per que no vagin massa rapid
							tempsFase = tempsFase * 100;

							try {
								getMagatzem().getSemaforoFabricantSimultaneament().acquire();
								fabricant = true;
								getMagatzem().getSemaforoDiariMoviments().acquire();
								// System.out.println("Permisos disponibles: "
								// + getMagatzem().getSemaforoFabricantSimultaneament().availablePermits());
							} catch (InterruptedException e1) {
								e1.printStackTrace();
							}
							getMagatzem().getMoviments()
									.add(new DiariMoviments(this.nomOperari + ": Trabajando en la comanda "
											+ comanda.getIdComanda() + ", he cogido " + fase.getQuantitat() + " "
											+ fase.getProducte().getUnitatMesura().toString() + " del ingrediente "
											+ fase.getProducte().getNomProducte() + ", tiempo de fabricacion: "
											+ tempsFase + "ms"));

							getMagatzem().getSemaforoDiariMoviments().release();

							/*
							 * System.out.println( this.nomOperari + ":Cogido " + fase.getQuantitat() +
							 * " unidades del ingrediente " + fase.getProducte().getNomProducte() +
							 * ", esperando: " + tempsFase);
							 */
							try {
								this.sleep(tempsFase);

							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							getMagatzem().getSemaforoFabricantSimultaneament().release();
							fabricant = false;
						}

						// Cuando acaba todas las fases a�ade el producto a la caja correspondiente,
						// esto le lleva entre 1 i 2 minutos
						int nRandom = (int) ((Math.random() * 1000) + 1000);
						try {
							this.sleep(nRandom);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}

						for (ComandaLinia cl : comanda.getLinies()) {
							if (cl.getProducte().getCodiProducte() == p.getCodiProducte()) {
								cl.setQuantitatPreparada(cl.getQuantitatPreparada() + 1);
								cl.setQuantitatPreparacio(cl.getQuantitatPreparacio() - 1);

								getMagatzem().getMoviments()
										.add(new DiariMoviments(this.getNomOperari() + ": " + p.getNomProducte()
												+ " fabricat per la comanda " + comanda.getIdComanda() + ", "
												+ p.getNomProducte() + "fabricats: " + cl.getQuantitatPreparada()
												+ " / " + cl.getQuantitatDemanada()));

								System.out.println(
										this.getNomOperari() + ": " + p.getNomProducte() + " fabricat per la comanda "
												+ comanda.getIdComanda() + ", " + p.getNomProducte() + "fabricats: "
												+ cl.getQuantitatPreparada() + " / " + cl.getQuantitatDemanada());
							}
						}

						// Comprabamos si hemos acabado de preparar la comanda
						boolean comandaTerminada = true;
						for (ComandaLinia cl : comanda.getLinies()) {
							if (cl.getQuantitatPreparada() < cl.getQuantitatDemanada()) {
								comandaTerminada = false;
							}
						}

						if (comandaTerminada) {
							comanda.setEstat(ComandaEstat.PREPARADA);
							System.out.println(this.nomOperari + ": comanda " + comanda.idComanda + " preparada");
							getMagatzem().getMoviments().add(new DiariMoviments(
									this.nomOperari + ": comanda " + comanda.idComanda + " preparada"));
						}

					} else {
						try {
							this.sleep(2000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				} else {
					try {
						this.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			} else {
				try {
					this.sleep(5000);
					descanso = false;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

		}
	}

	/**
	 *
	 * L'operari busca l'ingredient que necessita per poder completar la fase de
	 * produccio actual y si el troba "l'agafa" reduint l'stock en la quantitat
	 * adient
	 * 
	 * @param fase
	 *            el ProducteFabricacio en el que esta treballant l'operari
	 *
	 * @return true si ha trobat el ingredient false si no l'ha trobat
	 */
	private boolean agafarIngredients(ProducteFabricacio fase) {
		boolean ingredientTrobat = false;
		for (Producte ingredient : getMagatzem().getProductes()) {
			if (fase.getCodiProducte() == ingredient.getCodiProducte() && ingredient.getStock() > fase.getQuantitat()) {
				ingredientTrobat = true;
				ingredient.setStock(ingredient.getStock() - fase.getQuantitat());

				if (ingredient.getStock() <= ingredient.getStockMinim()) {
					boolean ingredientEnCami = false;
					try {
						getMagatzem().getSemaforoOrdresCompra().acquire();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					for (OrdreCompra oc : getMagatzem().getCompres()) {
						if (oc.getProducte().getCodiProducte() == ingredient.getCodiProducte()) {
							ingredientEnCami = true;
						}
					}
					if (!ingredientEnCami) {
						System.out.println("Nova ordre de compra de l'ingredient: " + ingredient.getNomProducte());
						getMagatzem().getCompres().add(new OrdreCompra(ingredient.getProveidor(), ingredient,
								ingredient.getQuantitatCompra()));
					}
					getMagatzem().getSemaforoOrdresCompra().release();
				}
			}
		}
		return ingredientTrobat;
	}

	public int getNumPastissos() {
		return numPastissos;
	}

	public void setNumPastissos(int numPastissos) {
		this.numPastissos = numPastissos;
	}

	@Override
	public String toString() {
		return (idOperari + ": " + getNomOperari());
	}

	public int getIdOperari() {
		return idOperari;
	}

	public String getNomOperari() {
		return nomOperari;
	}

	public void setNomOperari(String nomOperari) {
		this.nomOperari = nomOperari;
	}

	public Magatzem getMagatzem() {
		return Programa.getMagatzem();
	}

	public boolean isDescanso() {
		return descanso;
	}

	public void setDescanso(boolean descanso) {
		this.descanso = descanso;
	}

	public boolean isVol() {
		return vol;
	}

	public void setVol(boolean vol) {
		this.vol = vol;
	}

	public void setIdOperari(int idOperari) {
		this.idOperari = idOperari;
	}

	public boolean isFabricant() {
		return fabricant;
	}

	public void setFabricant(boolean fabricant) {
		this.fabricant = fabricant;
	}
}