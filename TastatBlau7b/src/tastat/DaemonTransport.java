package tastat;

public class DaemonTransport extends Thread {

	public int minutos = 0;
	private Magatzem mgz;

	public DaemonTransport(int minutos, Magatzem mgz) {
		this.minutos = minutos * 1000;
		this.mgz = mgz;
	}

	@SuppressWarnings("static-access")
	@Override
	public void run() {
		while (true) {
			boolean ple = false;
			try {
				this.sleep(minutos);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("****TRANSPORTE EN MARCHA****");
			// si hay alguna comanda en lista comandes la pasamos a lista preparació
			if (Programa.getMagatzem().getComandes() != null) {
				new DiariMoviments("El transport ha vingut.");
				for (int i = 0; i < mgz.getComandesEnPreparacio().size(); i++) {
					if (mgz.getComandesEnPreparacio().get(i).getEstat().equals(ComandaEstat.PREPARADA) && !ple) {
						mgz.getComandesFinalitzades().add(mgz.getComandesEnPreparacio().get(i));
						mgz.getComandesEnPreparacio().get(i).setEstat(ComandaEstat.TRANSPORT);
						System.out.println("El transport s'ha emportat la comanda:"
								+ mgz.getComandesEnPreparacio().get(i).getIdComanda());
						mgz.getMoviments().add(new DiariMoviments("El transport s'ha emportat la comanda:"
								+ mgz.getComandesEnPreparacio().get(i).getIdComanda()));
						mgz.getComandesEnPreparacio().remove(mgz.getComandesEnPreparacio().get(i));
						ple = true;
					}
				}
			}

			// Eliminamos comanda preparadas de la lista comandesEnPreparacio
			for (int i = 0; i < mgz.getComandesEnPreparacio().size(); i++) {
				if (!mgz.getComandesEnPreparacio().get(i).getEstat().equals(ComandaEstat.PENDENT)) {
					mgz.getComandesEnPreparacio().remove(mgz.getComandesEnPreparacio().get(i));
				}
			}

			// Buscamos comandas pendientes para reemplazar las que han acabado
			if (mgz.getComandesEnPreparacio().size() < mgz.MAXIM_COMANDES_EN_PREPARACIO) {
				for (int i = 0; i < mgz.getComandes().size(); i++) {
					if (mgz.getComandes().get(i).getEstat().equals(ComandaEstat.PENDENT)
							&& mgz.getComandesEnPreparacio().size() < mgz.MAXIM_COMANDES_EN_PREPARACIO) {

						mgz.getComandesEnPreparacio().add(mgz.getComandes().get(i));
					}
				}
			}
		}
	}
}
