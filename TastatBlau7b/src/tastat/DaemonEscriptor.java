package tastat;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class DaemonEscriptor extends Thread {
	List<DiariMoviments> diariList;
	String path = "DiariMoviments.txt";

	public DaemonEscriptor(List<DiariMoviments> list) {
		this.diariList = Collections.synchronizedList(list);
	}

	File arrayList2File(String path) {

		try {
			File f = new File(path);
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);

			try {
				Programa.getMagatzem().getSemaforoDiariMoviments().acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (diariList.size() > 0) {
				System.out.println("Escribiendo diario de movimientos");
				for (DiariMoviments dm : diariList) {
					if (dm != null) {
						bw.write(dm.toString());
						bw.newLine();
					}
				}
			}

			Programa.getMagatzem().getSemaforoDiariMoviments().release();

			bw.flush();
			bw.close();
			fw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;

	}

	@Override
	public void run() {
		while (true) {
			try {
				sleep(30000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			arrayList2File(path);
		}
	}
}
